var app = angular.module('FamilyEditor', ['ngRoute']);

var appInit = function () {};
var listInit = function () {};
var treeBuild = function () {};

app.directive('memberCard', function () {
	return {
		restrict: 'E',
		templateUrl: '/templates/member.html'
	};
});

app.controller('AppController', ['$http', function ($http) {
	var appCtrl = this;

	appCtrl.members = [];

	appCtrl.init = function () {
		$http
			.get('/member')
			.success(function (res) {
				appCtrl.members = res;
			});
	};

	appInit = appCtrl.init();
}]);

app.controller('FamilyListController', ['$http', function ($http) {
	var familyList = this;

	familyList.data = [];

	familyList.init = function () {
		$http
			.get('/family/find')
			.success(function (res) {
				familyList.data = res;
			});
	};

	listInit = familyList.init;

	familyList.addFamily = function () {
		$http
			.post('/family', {})
			.success(function () {
				familyList.init();
			});
	};

	familyList.addMember = function (familyID) {
		$http
			.post('/member', {family: familyID})
			.success(function () {
				familyList.init();
			});
	};

	familyList.delete = function (familyID) {
		$http
			.delete('/family/' + familyID)
			.success(function () {
				familyList.init();
			});
	};
}]);

app.controller('EditFamilyListController', ['$http', function ($http) {
	var editFamily = this;

	editFamily.show = false;

	editFamily.update = function (familyID, familyName) {
		$http
			.put('/family/' + familyID, {name: familyName})
			.success(function () {
				editFamily.show = false;
			});
	};
}]);

app.controller('MemberController', ['$http', function ($http) {
	var member = this;

	member.family = 0;
	member.father = 0;
	member.mother = 0;

	member.name = '';
	member.id = 0;

	member.children = [];
	member.siblings = [];

	member.showEditor = false;

	member.init = function (memberID) {
		memberID = memberID || member.id;

		$http
			.get('/member/' + memberID)
			.success(function (res) {
				member.family = res.family;
				member.father = res.father;
				member.mother = res.mother;
				member.name = res.name;
				member.id = res.id;

				member.getChildren();
				member.getSibling();
			});
	};

	member.getChildren = function () {
		$http
			.get('/member/children/' + member.id)
			.success(function (res) {
				member.children = res;
			});
	};

	member.getSibling = function () {
		$http
			.get('/member/sibling/' + member.id)
			.success(function (res) {
				member.siblings = res;
			});
	};

	member.update = function () {
		var updateData = {
			name: member.name,
			father: member.father,
			mother: member.mother,
			id: member.id
		};

		if (updateData.father === member.id.toString() || updateData.mother === member.id.toString()) {
			alert('Ошибка');
			location.reload();
			// member.father = 0;
			return;
		}

		$http
			.put('/member/' + member.id, updateData)
			.success(function () {
				member.showEditor = false;
				// member.init(member.id);
				listInit();
				appInit();
			})
			.error(function () {
				alert('Ошибка');
				location.reload();
			});
	};

	member.delete = function () {
		$http
			.delete('/member/' + member.id)
			.success(function () {
				listInit();
				appInit();
			});
	};

	member.tree = function () {
		treeBuild(member.id);
	};
}]);

app.controller('SearchController', ['$http', function ($http) {
	var search = this;

	search.query = '';
	search.data = [];

	search.find = function () {
		var buildedQuery = {
			where: {
				name: {contains: search.query}
			}
		};

		$http
			.post('/member/find', buildedQuery)
			.success(function (res) {
				if (res.length < 1) alert('По запросу ничего не найденно');
				search.data = res;

				// setTimeout(function () {
				// 	var searchResult = angular.element('#search-result');
				// 	searchResult.ready(function () {
						
				// 		var src_str = searchResult.html();
				// 		var term = search.query;
				// 		var pattern = new RegExp("("+term+")", "gi");

				// 		term = term.replace(/(\s+)/,"(<[^>]+>)*$1(<[^>]+>)*");

				// 		src_str = src_str.replace(pattern, "<mark>$1</mark>");
				// 		src_str = src_str.replace(/(<mark>[^<>]*)((<[^>]+>)+)([^<>]*<\/mark>)/,"$1</mark>$2<mark>$4");

				// 		searchResult.html(src_str);
				// 	});
				// }, 0);
			});
	};
}]);

app.controller('TreeController', ['$http', function ($http) {
	var tree = this;

	tree.show = false;
	tree.data = {};

	tree.build = function (memberID) {
		$http
			.get('/member/tree/' + memberID)
			.success(function (res) {
				tree.data = res;
				tree.show = true;
			});
	};

	treeBuild = tree.build;
}]);