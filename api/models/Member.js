/**
* Member.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var beforeUpdateCreate = function (values, next) {
	var memberID = values.id;

	console.log(memberID);

	sails.log(values);
	sails.log('-----');

	Member
		.findOne({id: memberID})
		.exec(function (err, member) {
			if (err) return next(err);

			if (values.mother || values.father) {
				if (values.mother && values.mother === memberID) return next('Can\'t be own parent');
				if (values.father && values.father === memberID) return next('Can\'t be own parent');
				if (values.father && values.father === member.mother) return next('Can\'t have two same parents');
				if (values.mother && values.mother === member.father) return next('Can\'t have two same parents');
				if (values.father === values.mother) return next('Can\'t add two same parents');

				return next();
			}
			
			return next();
		});
};

module.exports = {

	attributes: {
		family: {model: 'Family'},

		mother: {model: 'Member'},
		father: {model: 'Member'},
		
		name: {type: 'string'}
	},
	beforeUpdate: beforeUpdateCreate,
	beforeUpdateCreate: beforeUpdateCreate
};

