/**
 * MemberController
 *
 * @description :: Server-side logic for managing members
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

var async = require('async');
var _ = require('underscore');

var childrenCtrl = function (req, res) {
	var memberID = req.param('id') || 0;

	Member
		.find()
		.where({
			or: [{mother: memberID}, {father: memberID}]
		})
		.exec(function (err, children) {
			if (err) return res.send(500);

			res.send(children);
		});
};

var siblingCtrl = function (req, res) {
	var memberID = req.param('id') || 0;

	async.waterfall([
		function (callback) {
			Member
				.findOne({id: memberID})
				.exec(callback);
		},
		function (member, callback) {
			Member
				.find()
				.where({
					or: [{mother: member.mother}, {father: member.father}],
					id: {'!': member.id}
				})
				.exec(callback);
		}
	], function (err, siblings) {
		if (err) return res.send(500);

		res.send(siblings);
	});
};

var treeCtrl = function (req, res) {
	var memberID = req.param('id') || 0;
	var tree = [];
	var idLog = [];

	var recursiveTreeBuild = function (members, treeIndex, member_id, childID) {
		var member = _.find(members, function (member) {
			return member.id == member_id;
		});

		var hasMother = !!member.mother;
		var hasFather = !!member.father;

		if (idLog.length > 0 && idLog.indexOf(member_id) !== -1) return false;//console.log('ABORT! circular ' + member_id); // abort circular loop
		if (member_id) idLog.push(member_id);
		
		if (member_id === memberID) {
			tree[treeIndex] = {
				id: member.id,
				mother: member.mother,
				father: member.father,
				child: null,
				name: member.name
			};
		}
		else {
			tree[treeIndex] = tree[treeIndex] || [];
			tree[treeIndex][tree[treeIndex].length] = {
				id: member.id,
				mother: member.mother,
				father: member.father,
				child: childID,
				name: member.name
			};
		}

		treeIndex = treeIndex + 1;

		if (hasMother) recursiveTreeBuild(members, treeIndex, member.mother.id, member_id);
		if (hasFather) recursiveTreeBuild(members, treeIndex, member.father.id, member_id);
	};

	async.waterfall([
		function (callback) {
			Member
				.find({})
				.populateAll()
				.exec(callback);
		},
		function (members, callback) {
			recursiveTreeBuild(members, 0, memberID);

			callback(null, tree);
		}
	], function (err, tree) {
		if (err) return res.send(500);

		res.send(tree);
	});
};

module.exports = {
	children: childrenCtrl,
	sibling: siblingCtrl,
	tree: treeCtrl
};

